
# SoloGithub

App challenge where we have to show a Github repository list and a detail of the selected one.



## Tech Stack

### Tools
**Versions catalog** for libraries and plugins versioning

### Libraries
**Retrofit** for API calls

**Dagger-hilt** for dependency injection

**Glide** for image loading

**Coroutines** for asynchronous calls

**Mockk** for unit testing

**Paging** for pagination

**Timber** for logging

**Room** for local cache database

### App Architecture
I've used a Clean Architecture with 3 different layers: data, domain and presentation
The project packaging is organized by features, being as follows:
* _common_ - Package with common things used in both features.
* _repository_details_ - Feature representing the repo details screen.
* _repository_list_ - Feature representating the main screen of the app, the repositories list.

As presentation pattern, I've used MVVM for the RepositoryList screen and MVI for the RepositoryDetails one.

### Glossary
I usually follow these terms to define models in the different layers of the project:
* _entity_ for data layer models: i.e: _RepositoryEntity_
* _model_ for domain layer models: i.e: _RepositoryDetailsModel_
* _uiModel_ from presentation layer models: i.e: _RepositoryDetailsUiModel_

