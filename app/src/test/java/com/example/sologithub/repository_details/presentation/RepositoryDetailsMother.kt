package com.example.sologithub.repository_details.presentation

import com.example.sologithub.repository_details.domain.model.RepositoryDetailsModel

object RepositoryDetailsMother {

    val anyRepoDetail =  RepositoryDetailsModel(
        name = "name",
        description = "description",
        starsCount = 1,
        forksCount = 1,
        primaryLanguage = "Kotlin",
        url = "url"
    )
}