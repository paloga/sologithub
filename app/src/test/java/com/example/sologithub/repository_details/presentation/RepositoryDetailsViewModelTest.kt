package com.example.sologithub.repository_details.presentation

import androidx.lifecycle.SavedStateHandle
import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.common.tool.failure
import com.example.sologithub.common.tool.success
import com.example.sologithub.repository_details.domain.use_case.GetRepositoryDetailsUseCase
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewState
import com.example.sologithub.repository_details.presentation.ui_model.mapper.toUiModel
import com.example.sologithub.repository_list.common.test_utils.CoroutineTestRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RepositoryDetailsViewModelTest {

    @RelaxedMockK
    private lateinit var getRepositoryDetails: GetRepositoryDetailsUseCase

    @MockK
    private lateinit var savedStateHandle: SavedStateHandle

    private val sut by lazy {
        RepositoryDetailsViewModel(getRepositoryDetails, savedStateHandle)
    }

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        savedStateHandle.givenOwnerAndRepo()
    }

    @Test
    fun `WHEN viewModel initialized THEN Loading state is emitted`() {
        sut

        assertEquals(
            sut.viewState.value,
            RepositoryDetailsViewState.Loading
        )
    }

    @Test
    fun `GIVEN use case success WHEN viewModel initialized THEN WithData state is emitted`() {
        givenGetRepositoryDetailsSuccess()

        sut

        assertEquals(
            sut.viewState.value,
            RepositoryDetailsViewState.WithData(
                RepositoryDetailsMother.anyRepoDetail.toUiModel()
            )
        )
    }

    @Test
    fun `GIVEN use case failure WHEN viewModel initialized THEN Error state is emitted`() {
        givenGetRepositoryDetailsFailure()

        sut

        assertEquals(
            sut.viewState.value,
            RepositoryDetailsViewState.Error
        )
    }

    private fun SavedStateHandle.givenOwnerAndRepo() {
        every {
            get<String>("owner_name")
        } returns "owner name"
        every {
            get<String>("repository_name")
        } returns "repo name"
    }

    private fun givenGetRepositoryDetailsSuccess() {
        coEvery { getRepositoryDetails(any(), any()) } returns
            RepositoryDetailsMother.anyRepoDetail.success()
    }

    private fun givenGetRepositoryDetailsFailure() {
        coEvery { getRepositoryDetails(any(), any()) } returns
            SoloGithubError.Generic("Unknow error").failure()
    }
}