package com.example.sologithub.repository_list.presentation

import androidx.paging.LoadState
import app.cash.turbine.test
import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.repository_list.common.test_utils.CoroutineTestRule
import com.example.sologithub.repository_list.domain.use_case.FetchRepositoryListUseCase
import com.example.sologithub.repository_list.presentation.ui_model.RepositoryUiModel
import com.example.sologithub.repository_list.presentation.ui_model.mapper.RepositoryViewState
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RepositoryListViewModelTest {

    @RelaxedMockK
    private lateinit var fetchRepositoryList: FetchRepositoryListUseCase

    private lateinit var sut: RepositoryListViewModel

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        sut = RepositoryListViewModel(fetchRepositoryList)
    }

    @Test
    fun `WHEN searchRepos THEN verify use case is called`() = runTest {
        sut.searchRepos()

        verify(exactly = 1) { fetchRepositoryList() }
    }

    @Test
    fun `GIVEN loadState is InternetNotAvailable WHEN onLoadedStateChanged THEN viewState is NoInternetError`() = runTest {
        sut.onLoadedStateChanged(LoadState.Error(SoloGithubError.InternetNotAvailable))

        sut.viewState.test {
            assertEquals(awaitItem(), RepositoryViewState.NoInternetError)
        }
    }

    @Test
    fun `GIVEN loadState is not InternetNotAvailable WHEN onLoadedStateChanged THEN viewState is DefaultError`() = runTest {
        sut.onLoadedStateChanged(LoadState.Error(SoloGithubError.Generic("Unknown Error")))

        sut.viewState.test {
            assertEquals(awaitItem(), RepositoryViewState.DefaultError)
        }
    }

    @Test
    fun `GIVEN loadState is Loading WHEN onLoadedStateChanged THEN viewState is Loading`() = runTest {
        sut.onLoadedStateChanged(LoadState.Loading)

        sut.viewState.test {
            assertEquals(awaitItem(), RepositoryViewState.Loading)
        }
    }

    @Test
    fun `GIVEN loadState is NotLoading WHEN onLoadedStateChanged THEN viewState is Data`() = runTest {
        sut.onLoadedStateChanged(LoadState.NotLoading(endOfPaginationReached = true))

        sut.viewState.test {
            assertEquals(awaitItem(), RepositoryViewState.Data)
        }
    }

    @Test
    fun `GIVEN repository WHEN onRepositoryClicked THEN navigateToRepositoryDetails is repository`() = runTest {
        val repository = RepositoryUiModel("name", "description", "owner", "25")
        sut.onRepositoryClicked(repository)

        sut.navigateToRepositoryDetails.test {
            assertEquals(awaitItem(), repository)
        }
    }

    @Test
    fun `WHEN onRepositoryDetailsNavigated THEN navigateToRepositoryDetails is null`() = runTest {
        sut.onRepositoryDetailsNavigated()

        sut.navigateToRepositoryDetails.test {
            assertEquals(awaitItem(), null)
        }
    }
}