package com.example.sologithub.repository_list.common.test_utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.rules.TestWatcher
import org.junit.runner.Description

/**
 * CoroutineTestRule can be used to automatically replace the [Dispatchers.Main]
 * with a TestDispatcher and reset it after the execution of all the tests.
 *
 * The default [TestDispatcher] used is the [UnconfinedTestDispatcher] but is possible
 * to specify to use a different TestDispatcher, like the [StandardTestDispatcher],
 * passing it in the constructor of the Rule.
 *
 * The rule will provide an instance of the [TestCoroutineDispatchers] that can be used to
 * pass the [CoroutineDispatchers] in all the class that are using it to replace the
 * [Dispatchers] via dependency injection.
 *
 * The rule will also provide a [TestScope] that can be used if needed.
 *
 * Some example of how to use the Rule
 *
 * Rule declaration:
 *
 * ```
 *  @get:Rule
 *  val coroutineTestRule = CoroutineTestRule()
 *  // or
 *  val coroutineTestRule = CoroutineTestRule(StandardTestDispatcher())
 *  // if you want to use the StandardTestDispatcher
 * ```
 *
 *  You can then use the coroutineTestRule val to get the coroutineDispatchers or testScope if needed
 *
 * To run a test simply use the [runTest]
 * ```
 * @Test
 * fun usingRunTest() = runTest {
 *     aTestCoroutine()
 * }
 * ```
 * > Note: This rule is not compatible with Junit 5. For Junit 5 support use [TestCoroutineExtension]
 *
 * @param dispatcher if provided, otherwise [UnconfinedTestDispatcher] will be used.
 * @see Dispatchers
 * @see Dispatchers.Main
 * @see TestCoroutineDispatchers
 * @see runTest
 * @see TestDispatcher
 */
@ExperimentalCoroutinesApi
class CoroutineTestRule(dispatcher: CoroutineDispatcher = UnconfinedTestDispatcher()) : TestWatcher() {

    val coroutineDispatchers = TestCoroutineDispatchers(dispatcher)

    val testScope = TestScope(coroutineDispatchers.main)

    override fun starting(description: Description) {
        super.starting(description)
        Dispatchers.setMain(coroutineDispatchers.main)
    }

    override fun finished(description: Description) {
        super.finished(description)
        Dispatchers.resetMain()
    }
}
