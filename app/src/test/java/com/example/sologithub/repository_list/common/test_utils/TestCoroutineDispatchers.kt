package com.example.sologithub.repository_list.common.test_utils

import com.example.sologithub.common.tool.coroutines.CoroutineDispatchers
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher

@ExperimentalCoroutinesApi
class TestCoroutineDispatchers internal constructor(
    dispatcher: CoroutineDispatcher = UnconfinedTestDispatcher()
) : CoroutineDispatchers {

    override val default: CoroutineDispatcher = dispatcher
    override val main: CoroutineDispatcher = dispatcher
    override val io: CoroutineDispatcher = dispatcher
}