package com.example.sologithub.common.tool

sealed class SoloGithubResult<out E : Throwable, out S> {

    data class Success<out S>(val value: S) : SoloGithubResult<Nothing, S>()

    data class Failure<out E : Throwable>(val exception: E) : SoloGithubResult<E, Nothing>()

    /**
     * Boolean property to know if result was a success
     */
    val isSuccess
        get() = this is Success

    /**
     * Boolean property to know if result was a failure
     */
    val isFailure
        get() = this is Failure

    fun exceptionOrNull(): E? =
        when (this) {
            is Failure -> exception
            else -> null
        }

    fun getOrNull(): S? =
        when (this) {
            is Success -> value
            else -> null
        }

    fun getOrThrow(): S =
        when (this) {
            is Success -> value
            is Failure -> throw exception
        }

    override fun toString(): String =
        when (this) {
            is Success -> "SoloGithubResult.Success($this)"
            is Failure -> "SoloGithubResult.Failure($this)"
        }

    companion object {
        fun <E : Throwable> failure(exception: E): SoloGithubResult<E, Nothing> = Failure(exception)

        fun <S> success(value: S): SoloGithubResult<Nothing, S> = Success(value)
    }
}