package com.example.sologithub.common.network.model

import java.io.IOException

class NoInternetAccessException : IOException() {
    override val message: String
        get() = "There is not internet access"
}