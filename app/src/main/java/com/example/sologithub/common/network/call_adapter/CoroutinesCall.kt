package com.example.sologithub.common.network.call_adapter

import com.example.sologithub.common.network.deserializer.customConfig
import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.network.model.SoloGithubApiErrorBody
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.common.tool.failure
import com.example.sologithub.common.tool.runTrying
import java.io.IOException
import java.lang.reflect.Type
import java.net.HttpURLConnection.HTTP_NO_CONTENT
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

internal class CoroutinesCall<T>(
    private val proxyCall: Call<T>,
    private val successType: Type
) : Call<SoloGithubResult<SoloGithubApiError, T>> {

    override fun enqueue(callback: Callback<SoloGithubResult<SoloGithubApiError, T>>) =
        proxyCall.enqueue(
            object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    when {
                        response.isSuccessful -> onSuccessfulResponse(response)
                        else -> onFailedResponse(response)
                    }
                }

                override fun onFailure(call: Call<T>, throwable: Throwable) {
                    callback.notifyResult(SoloGithubApiError(throwable).failure())
                }

                @Suppress("UNCHECKED_CAST")
                private fun onSuccessfulResponse(response: Response<T>) = with(response) {
                    val body = body()
                    val result = when {
                        successType != Unit::class.java && code() == HTTP_NO_CONTENT -> SoloGithubResult.failure(
                            SoloGithubApiError(IOException("No content response must be declared used Unit"))
                        )
                        body == null && code() != HTTP_NO_CONTENT -> SoloGithubResult.failure(
                            SoloGithubApiError(IllegalStateException("Response body was null"))
                        )
                        else -> SoloGithubResult.success(body ?: Unit as T)
                    }
                    callback.notifyResult(result)
                }

                private fun onFailedResponse(response: Response<T>) = with(response) {
                    val errorJson = errorBody()?.string() ?: ""
                    val errorBody = runTrying {
                        customConfig.decodeFromString<SoloGithubApiErrorBody>(errorJson)
                    }.getOrNull()
                    val result = SoloGithubResult.failure(
                        SoloGithubApiError(code(), errorJson, errorBody)
                    )
                    callback.notifyResult(result)
                }
            }
        )

    override fun execute(): Response<SoloGithubResult<SoloGithubApiError, T>> {
        throw UnsupportedOperationException("Coroutines call does not support synchronous execution")
    }

    override fun isCanceled(): Boolean = proxyCall.isCanceled

    override fun cancel() = proxyCall.cancel()

    override fun isExecuted(): Boolean = proxyCall.isExecuted

    override fun clone(): Call<SoloGithubResult<SoloGithubApiError, T>> = CoroutinesCall(proxyCall.clone(), successType)

    override fun request(): Request = proxyCall.request()

    override fun timeout(): Timeout = proxyCall.timeout()

    internal fun Callback<SoloGithubResult<SoloGithubApiError, T>>.notifyResult(result: SoloGithubResult<SoloGithubApiError, T>) =
        onResponse(this@CoroutinesCall, Response.success(result))
}
