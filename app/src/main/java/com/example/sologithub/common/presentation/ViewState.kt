package com.example.sologithub.common.presentation

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

interface ViewState

interface ViewModelStates<VS : ViewState> {

    val viewState: StateFlow<VS>

    fun updateState(newState: VS)
    fun updateState(function: (VS) -> VS)
}

class ViewModelStatesImpl<VS : ViewState>(initialState: VS) : ViewModelStates<VS> {

    private val _viewState: MutableStateFlow<VS> = MutableStateFlow(initialState)
    override val viewState: StateFlow<VS> get() = _viewState

    override fun updateState(newState: VS) {
        _viewState.value = newState
    }
    override fun updateState(function: (VS) -> VS) {
        _viewState.update(function)
    }
}