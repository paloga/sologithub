package com.example.sologithub.common.presentation

interface ViewAction

fun interface ViewModelActions<VA : ViewAction> {

    fun onViewAction(action: VA)
}
