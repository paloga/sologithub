package com.example.sologithub.common.network.model

import java.io.IOException

sealed class SoloGithubApiError private constructor(
    val msg: String,
    cause: Throwable? = null
) : Throwable(msg, cause) {

    /**
     * Server returned error response.
     *
     * @param code HTTP status code returned by the server.
     * @param jsonServerResponse Server response.
     * @param body Error response body in [SoloGithubApiErrorBody] format. It is usually returned with error codes 4xx and 5xx.
     *             Body will be null if the error response is not supported by [SoloGithubApiErrorBody] format. In that case,
     *             you can use [jsonServerResponse] to parse the error response.
     */
    data class HttpError(val code: Int, val jsonServerResponse: String, val body: SoloGithubApiErrorBody? = null) :
        SoloGithubApiError(jsonServerResponse)

    /**
     * An [IOException] occurred while communicating to the server.
     * This usually occurs when device has no internet connection.
     */
    data class NetworkError(val exception: IOException) :
        SoloGithubApiError(exception.message ?: "Network error", exception)

    data class UnknownError(val exception: Throwable) :
        SoloGithubApiError(exception.message ?: "Unknown error", exception)
}

fun SoloGithubApiError(throwable: Throwable): SoloGithubApiError =
    when (throwable) {
        is IOException -> SoloGithubApiError.NetworkError(throwable)
        else -> SoloGithubApiError.UnknownError(throwable)
    }

fun SoloGithubApiError(code: Int, json: String, body: SoloGithubApiErrorBody? = null): SoloGithubApiError =
    SoloGithubApiError.HttpError(code, json, body)