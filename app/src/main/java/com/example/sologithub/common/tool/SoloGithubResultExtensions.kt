@file:OptIn(ExperimentalContracts::class, ExperimentalContracts::class)

package com.example.sologithub.common.tool

import kotlinx.coroutines.CancellationException
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * This function is going to be used as a replacement of [runCatching] natively available in kotlin.
 * The problem of runCatching is that is catching all types of exceptions and is not handling cases where
 * these exceptions must be propagated.
 *
 * In the same way of [runCatching] this function is calling the specified function block and returns its encapsulated
 * in [Result] Type
 *
 * In case of an exception of type [CancellationException] this will NOT be catched from this function.
 *
 * @see runCatching
 * @see SoloGithubResult
 * @see CancellationException,
 * */
inline fun <S> runTrying(block: () -> S): SoloGithubResult<Throwable, S> =
    try {
        val result = block()
        SoloGithubResult.success(result)
    } catch (cancellation: CancellationException) {
        throw cancellation // Throw CancellationException to don't break the cancellation flow of coroutines
    } catch (e: Throwable) {
        SoloGithubResult.failure(e)
    }

/**
 * Method to build a success SoloGithubResult
 */
fun <Input> Input.success() = SoloGithubResult.success(this)

/**
 * Method to build a failure SoloGithubResult
 */
fun <Input : Throwable> Input.failure() = SoloGithubResult.failure(this)

/**
 * Method to obtain and control the failure and the success cases.
 */
inline fun <E : Throwable, S, T> SoloGithubResult<E, S>.fold(
    left: (E) -> T,
    right: (S) -> T
): T {
    contract {
        callsInPlace(left, InvocationKind.AT_MOST_ONCE)
        callsInPlace(right, InvocationKind.AT_MOST_ONCE)
    }
    return when (this) {
        is SoloGithubResult.Failure -> left(exception)
        is SoloGithubResult.Success -> right(value)
    }
}

inline fun <E : Throwable, S, R> SoloGithubResult<E, S>.flatMap(
    transform: (S) -> SoloGithubResult<E, R>
): SoloGithubResult<E, R> {
    contract { callsInPlace(transform, InvocationKind.AT_MOST_ONCE) }
    return fold(right = transform, left = { SoloGithubResult.failure(it) })
}

inline fun <E : Throwable, S, R> SoloGithubResult<E, S>.map(
    transform: (S) -> R
): SoloGithubResult<E, R> {
    contract { callsInPlace(transform, InvocationKind.AT_MOST_ONCE) }
    return flatMap { SoloGithubResult.success(transform(it)) }
}

fun <E : Throwable, S> SoloGithubResult<E, S>.mapToUnit(): SoloGithubResult<E, Unit> =
    flatMap { SoloGithubResult.success(Unit) }

inline fun <E : Throwable, S, E2 : Throwable> SoloGithubResult<E, S>.mapFailure(
    transform: (E) -> E2
): SoloGithubResult<E2, S> {
    contract { callsInPlace(transform, InvocationKind.AT_MOST_ONCE) }
    return flatMapFailure { SoloGithubResult.failure(transform(it)) }
}

inline fun <E : Throwable, S, R : Throwable> SoloGithubResult<E, S>.flatMapFailure(
    transform: (E) -> SoloGithubResult<R, S>
): SoloGithubResult<R, S> {
    contract { callsInPlace(transform, InvocationKind.AT_MOST_ONCE) }
    return fold(right = { SoloGithubResult.success(it) }, left = transform)
}

inline fun <E : Throwable, S> SoloGithubResult<E, S>.onSuccess(
    action: (value: S) -> Unit
): SoloGithubResult<E, S> {
    contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
    if (this is SoloGithubResult.Success) action(value)
    return this
}

inline fun <E : Throwable, S> SoloGithubResult<E, S>.onFailure(
    action: (exception: E) -> Unit
): SoloGithubResult<E, S> {
    contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
    if (this is SoloGithubResult.Failure) action(exception)
    return this
}
