package com.example.sologithub.common.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map

interface ViewEvent

interface ViewModelEvents<VE : ViewEvent> {

    val viewEvent: Flow<VE?>

    fun consumeEvent(event: VE?, eventToConsumeCallback: (VE) -> Unit)

    fun addEvent(event: VE)
}

open class ViewModelEventsImpl<VE : ViewEvent> : ViewModelEvents<VE> {

    private val _viewEvents: MutableStateFlow<List<VE>> = MutableStateFlow(listOf())
    override val viewEvent: Flow<VE?>
        get() = _viewEvents.map { events -> events.firstOrNull() }

    override fun consumeEvent(event: VE?, eventToConsumeCallback: (VE) -> Unit) {
        event?.let {
            eventToConsumeCallback(it)
            removeEvent(it)
        }
    }

    override fun addEvent(event: VE) {
        _viewEvents.value = _viewEvents.value + event
    }

    private fun removeEvent(event: VE) {
        _viewEvents.value = _viewEvents.value - event
    }
}
