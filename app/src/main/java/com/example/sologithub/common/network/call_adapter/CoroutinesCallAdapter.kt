package com.example.sologithub.common.network.call_adapter

import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

internal class CoroutinesCallAdapter<T : Any>(
    private val successType: Type
) : CallAdapter<T, Call<SoloGithubResult<SoloGithubApiError, T>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<T>): Call<SoloGithubResult<SoloGithubApiError, T>> = CoroutinesCall(call, successType)
}