package com.example.sologithub.common.network.deserializer

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json

@OptIn(ExperimentalSerializationApi::class)
internal val customConfig: Json
    get() = Json {
        prettyPrint = true
        explicitNulls = false
        ignoreUnknownKeys = true
    }