package com.example.sologithub.common.network.interceptor

import android.net.ConnectivityManager
import android.os.Build

internal fun ConnectivityManager.isNetworkAvailable(): Boolean =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = getNetworkCapabilities(activeNetwork)
        // If networkCapabilities is not null, proceed with capability checks
        networkCapabilities != null &&
            networkCapabilities.hasCapability(android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET) &&
            networkCapabilities.hasCapability(android.net.NetworkCapabilities.NET_CAPABILITY_VALIDATED)
    } else {
        // For API levels below 23, check if there is any active network
        activeNetworkInfo?.isConnectedOrConnecting == true
    }