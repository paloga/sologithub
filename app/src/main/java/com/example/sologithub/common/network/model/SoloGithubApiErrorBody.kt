package com.example.sologithub.common.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SoloGithubApiErrorBody internal constructor(
    @SerialName("errors") private val errors: List<InternalError> = emptyList()
) {

    constructor(code: String, detail: String) : this(listOf(InternalError(code, detail)))

    val code: String
        get() = errors.firstOrNull()?.code.orEmpty()

    val detail: String
        get() = errors.firstOrNull()?.detail.orEmpty()
}

@Serializable
internal data class InternalError(
    @SerialName("code") val code: String? = null,
    @SerialName("detail") val detail: String
)