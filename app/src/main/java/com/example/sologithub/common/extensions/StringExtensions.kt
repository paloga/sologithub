package com.example.sologithub.common.extensions

fun String.cleanDescription(): String {
    val maxLength = 500
    return if (this.length > maxLength) {
        this.substring(0, maxLength) + "..."
    } else {
        this
    }
}