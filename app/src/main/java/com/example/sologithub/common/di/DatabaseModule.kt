package com.example.sologithub.common.di

import android.content.Context
import androidx.room.Room
import com.example.sologithub.common.database.SoloGithubDb
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext applicationContext: Context): SoloGithubDb =
        Room.databaseBuilder(
            applicationContext,
            SoloGithubDb::class.java,
            SoloGithubDb.DATABASE_NAME
        )
            .build()
}