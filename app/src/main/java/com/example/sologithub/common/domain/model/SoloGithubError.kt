package com.example.sologithub.common.domain.model

/**
 * Class used to define the most common errors that the app can handle
 */
sealed class SoloGithubError(msg: String) : Exception(msg) {
    class Generic(msg: String) : SoloGithubError(msg)
    data object InternetNotAvailable :
        SoloGithubError("Internet connectivity not available in the device in this moment")
}