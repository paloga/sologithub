package com.example.sologithub.common.network

import android.content.Context
import com.example.sologithub.BuildConfig
import com.example.sologithub.common.network.call_adapter.CoroutinesCallAdapterFactory
import com.example.sologithub.common.network.deserializer.customConfig
import com.example.sologithub.common.network.interceptor.ConnectivityInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SoloGithubRetrofitClient @Inject constructor(
    applicationContext: Context
) {

    private val okHttpClient: OkHttpClient by lazy {
        OkHttpClient()
            .newBuilder()
            .addInterceptor(getHttpLoginInterceptor())
            .addInterceptor(ConnectivityInterceptor(applicationContext))
            .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    private fun getHttpLoginInterceptor() = HttpLoggingInterceptor().apply {
        level = when (BuildConfig.DEBUG) {
            true -> HttpLoggingInterceptor.Level.BODY
            false -> HttpLoggingInterceptor.Level.NONE
        }
    }

    val createApi: SoloGithubApi by lazy {
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(customConfig.asConverterFactory(JSON_CONTENT_TYPE.toMediaType()))
            .addCallAdapterFactory(CoroutinesCallAdapterFactory())
            .client(okHttpClient)
            .build()
            .create(SoloGithubApi::class.java)
    }

    companion object {
        private const val JSON_CONTENT_TYPE = "application/json"
        private const val TIMEOUT_IN_SECONDS = 60L
    }
}
