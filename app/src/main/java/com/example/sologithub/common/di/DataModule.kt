package com.example.sologithub.common.di

import android.content.Context
import com.example.sologithub.common.network.SoloGithubApi
import com.example.sologithub.common.network.SoloGithubRetrofitClient
import com.example.sologithub.common.tool.coroutines.CoroutineDispatchers
import com.example.sologithub.common.tool.coroutines.CoroutineDispatchersProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideApiClient(@ApplicationContext context: Context): SoloGithubApi =
        SoloGithubRetrofitClient(context).createApi

    @Provides
    fun provideDispatchers(): CoroutineDispatchers = CoroutineDispatchersProvider()

}