package com.example.sologithub.common.network.model.mapper

import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.common.network.model.NoInternetAccessException
import com.example.sologithub.common.network.model.SoloGithubApiError
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

fun SoloGithubApiError.toModel(): SoloGithubError =
    when (this) {
        is SoloGithubApiError.NetworkError ->
            if (exception is NoInternetAccessException) {
                SoloGithubError.InternetNotAvailable
            } else {
                SoloGithubError.Generic(msg)
            }

        is SoloGithubApiError.HttpError -> SoloGithubError.Generic(
            msg = jsonServerResponse.jsonToErrorMsg()
        )

        is SoloGithubApiError.UnknownError -> SoloGithubError.Generic(msg)
    }

private fun String.jsonToErrorMsg(): String {
    return try {
        val jsonConfig = Json { ignoreUnknownKeys = true }
        jsonConfig.decodeFromString<JsonHttpError>(this).message
    } catch (exception: Exception) {
        this
    }
}

@Serializable
private data class JsonHttpError(
    @SerialName("message") val message: String
)