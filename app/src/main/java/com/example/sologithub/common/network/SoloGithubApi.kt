package com.example.sologithub.common.network

import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import com.example.sologithub.repository_list.data.entity.RepositoryListEntity
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SoloGithubApi {

    @GET("/search/repositories?q=language=kotlin&sort=stars&order=desc")
    suspend fun fetchRepositories(
        @Query("page") page: Int? = null
    ): SoloGithubResult<SoloGithubApiError, RepositoryListEntity>

    @GET("/repos/{owner}/{name}")
    suspend fun getRepositoryDetails(
        @Path("owner") owner: String,
        @Path("name") name: String
    ): SoloGithubResult<SoloGithubApiError, RepositoryEntity>

    @GET("/repos/{owner}/{name}/languages")
    suspend fun getRepositoryLanguages(
        @Path("owner") owner: String,
        @Path("name") name: String
    ): SoloGithubResult<SoloGithubApiError, Map<String, Long>>
}

