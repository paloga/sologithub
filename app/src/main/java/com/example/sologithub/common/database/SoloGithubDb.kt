package com.example.sologithub.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.sologithub.common.database.SoloGithubDb.Companion.DATABASE_VERSION
import com.example.sologithub.repository_list.data.database.RepositoryDao
import com.example.sologithub.repository_list.data.database.RepositoryDto
import com.example.sologithub.repository_list.data.database.RepositoryPageDao
import com.example.sologithub.repository_list.data.database.RepositoryPageDto


@Database(
    version = DATABASE_VERSION,
    entities = [RepositoryDto::class, RepositoryPageDto::class]
)

abstract class SoloGithubDb : RoomDatabase() {

    abstract fun getRepositoryDao(): RepositoryDao

    abstract fun getRepositoryPageDao(): RepositoryPageDao

    companion object {
        const val DATABASE_NAME = "SoloGithubDatabase"
        internal const val DATABASE_VERSION = 1
    }
}
