package com.example.sologithub.repository_list.di

import com.example.sologithub.repository_list.data.datasource.ApiRepositoryListDataSource
import com.example.sologithub.repository_list.data.datasource.RoomRepositoryListDataSource
import com.example.sologithub.repository_list.data.repository.RepositoryListDataRepository
import com.example.sologithub.repository_list.domain.repository.RepositoryListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class RepositoryListDataModule {

    @Provides
    @Singleton
    fun provideRepositoryListRepository(
        apiDataSource: ApiRepositoryListDataSource,
        roomDataSource: RoomRepositoryListDataSource
    ): RepositoryListRepository = RepositoryListDataRepository(apiDataSource, roomDataSource)

}