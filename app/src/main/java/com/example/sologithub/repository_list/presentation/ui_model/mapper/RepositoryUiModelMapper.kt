package com.example.sologithub.repository_list.presentation.ui_model.mapper

import com.example.sologithub.repository_list.domain.model.RepositoryModel
import com.example.sologithub.repository_list.presentation.ui_model.RepositoryUiModel

fun RepositoryModel.toUiModel() =
    RepositoryUiModel(
        name = name,
        description = description,
        stars = stars.toString(),
        ownerName = ownerName
    )