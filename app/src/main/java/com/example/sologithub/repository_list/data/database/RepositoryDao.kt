package com.example.sologithub.repository_list.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RepositoryDao {

    @Query("SELECT * FROM repositories WHERE page = :pagePosition")
    suspend fun getRepositoriesByPage(pagePosition: Int): List<RepositoryDto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg repositories: RepositoryDto)

    @Query("DELETE FROM repositories")
    suspend fun deleteAll()
}