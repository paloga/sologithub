package com.example.sologithub.repository_list.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.sologithub.repository_list.data.database.RepositoryPageDto.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class RepositoryPageDto(
    @PrimaryKey @ColumnInfo(name = "page") val pagePosition: Int,
    @ColumnInfo(name = "valid_to") val validToInMillis: Long,
) {
    companion object {
        internal const val TABLE_NAME = "repository_pages"
    }
}

