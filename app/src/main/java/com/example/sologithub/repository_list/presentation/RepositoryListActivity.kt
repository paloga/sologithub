package com.example.sologithub.repository_list.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sologithub.R
import com.example.sologithub.databinding.ActivityRepositoryListBinding
import com.example.sologithub.repository_details.presentation.RepositoryDetailsActivity
import com.example.sologithub.repository_list.presentation.adapter.LoadMoreAdapter
import com.example.sologithub.repository_list.presentation.adapter.RepositoryListAdapter
import com.example.sologithub.repository_list.presentation.ui_model.mapper.RepositoryViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber


@AndroidEntryPoint
class RepositoryListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRepositoryListBinding

    private val repositoryListAdapter: RepositoryListAdapter by lazy {
        RepositoryListAdapter { repository ->
            viewModel.onRepositoryClicked(repository)
        }
    }

    private val loadMoreAdapter: LoadMoreAdapter by lazy {
        LoadMoreAdapter { repositoryListAdapter.retry() }
    }

    private val viewModel: RepositoryListViewModel by viewModels()

    private var searchJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepositoryListBinding.inflate(layoutInflater)

        setContentView(binding.root)

        loadGithubLogo()
        initRecyclerView()

        observeLoadState()
        observeNavigation()
        searchRepos()
    }

    private fun initRecyclerView() {
        with(binding.rcvChargers) {
            layoutManager = LinearLayoutManager(this@RepositoryListActivity)
            addItemDecoration(DividerItemDecoration(this@RepositoryListActivity, DividerItemDecoration.VERTICAL))
            adapter = repositoryListAdapter.withLoadStateFooter(
                footer = LoadMoreAdapter { loadMoreAdapter }
            )
        }
    }

    private fun loadGithubLogo() {
        val options: RequestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide.with(this)
            .load(R.drawable.github_logo)
            .apply(options)
            .into(binding.imgLogo)
    }

    private fun observeLoadState() {
        Timber.d("PABLOKU observeLoadState")
        lifecycleScope.launch {
            repositoryListAdapter.loadStateFlow.collectLatest { loadStates ->
                viewModel.onLoadedStateChanged(loadStates.refresh)
            }
        }
        lifecycleScope.launch {
            viewModel.viewState.collectLatest { viewState ->
                when (viewState) {
                    RepositoryViewState.Loading -> showLoading()
                    RepositoryViewState.Data -> showData()
                    RepositoryViewState.DefaultError -> showDefaultError()
                    RepositoryViewState.NoInternetError -> showNoInternetError()
                }
            }
        }
    }

    private fun searchRepos() {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.searchRepos()
                .collect {
                    repositoryListAdapter.submitData(it)
                }
        }
    }


    private fun observeNavigation() {
        lifecycleScope.launch {
            viewModel.navigateToRepositoryDetails.collect { repository ->
                repository?.let {
                    RepositoryDetailsActivity.build(this@RepositoryListActivity, repository.ownerName, repository.name)
                        .also {
                            startActivity(it)
                        }

                    viewModel.onRepositoryDetailsNavigated()
                }
            }
        }
    }

    private fun showData() {
        Timber.d("PABLOKU showData")
        with(binding) {
            rcvChargers.isVisible = true
            pbLoading.isVisible = false
            errorView.isVisible = false
        }
    }

    private fun showLoading() {
        Timber.d("PABLOKU showLoading")
        with(binding) {
            rcvChargers.isVisible = false
            pbLoading.isVisible = true
            errorView.isVisible = false
        }
    }

    private fun showDefaultError() {
        Timber.d("PABLOKU showDefaultError")
        with(binding) {
            rcvChargers.isVisible = false
            pbLoading.isVisible = false
            errorView.isVisible = true
        }
    }

    private fun showNoInternetError() {
        Timber.d("PABLOKU showNoInternetError")
        with(binding) {
            rcvChargers.isVisible = false
            pbLoading.isVisible = false
            errorView.isVisible = true
            errorView.text = "No internet connection. Please, check your internet connection and try again"
        }
    }

}