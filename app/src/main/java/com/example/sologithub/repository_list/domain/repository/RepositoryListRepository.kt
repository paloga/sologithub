package com.example.sologithub.repository_list.domain.repository

import androidx.paging.PagingSource
import com.example.sologithub.repository_list.domain.model.RepositoryModel

interface RepositoryListRepository {

    fun fetchPaginatedRepositories(): PagingSource<Int, RepositoryModel>

}