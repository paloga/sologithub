package com.example.sologithub.repository_list.data.datasource

import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.repository_list.data.entity.RepositoryListEntity

interface RepositoryListDatasource {

    suspend fun fetchRepositories(page: Int): SoloGithubResult<SoloGithubApiError, RepositoryListEntity>
}