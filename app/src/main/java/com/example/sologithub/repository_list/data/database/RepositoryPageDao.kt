package com.example.sologithub.repository_list.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RepositoryPageDao {

    @Query(value = "SELECT * FROM repository_pages WHERE page >= :pagePosition")
    suspend fun getPage(pagePosition: Int): RepositoryPageDto?

    @Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    suspend fun insertPage(repositoryPage: RepositoryPageDto)

    @Query(value = "DELETE FROM repository_pages")
    suspend fun deleteAll()
}