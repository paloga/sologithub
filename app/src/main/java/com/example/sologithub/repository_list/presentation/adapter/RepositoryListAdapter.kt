package com.example.sologithub.repository_list.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.sologithub.databinding.RowRepositoryBinding
import com.example.sologithub.repository_list.presentation.ui_model.RepositoryUiModel

class RepositoryListAdapter(
    private val onRepositoryClickListener: (repository: RepositoryUiModel) -> Unit
) : PagingDataAdapter<RepositoryUiModel, RepositoryListAdapter.RepositoryViewHolder>(RepositoryDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val binding = RowRepositoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RepositoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class RepositoryViewHolder(private val binding: RowRepositoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(repository: RepositoryUiModel?) {
            with(binding) {
                txtName.text = repository?.name
                txtDescription.text = repository?.description
                txtStars.text = repository?.stars

                repository?.let {
                    root.setOnClickListener {
                        onRepositoryClickListener(repository)
                    }
                }
            }
        }
    }

    class RepositoryDiffCallback : DiffUtil.ItemCallback<RepositoryUiModel>() {
        override fun areItemsTheSame(oldItem: RepositoryUiModel, newItem: RepositoryUiModel): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: RepositoryUiModel, newItem: RepositoryUiModel): Boolean {
            return oldItem == newItem
        }
    }
}