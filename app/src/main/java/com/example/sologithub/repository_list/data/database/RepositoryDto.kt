package com.example.sologithub.repository_list.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.sologithub.repository_list.data.database.RepositoryDto.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class RepositoryDto(
    @PrimaryKey @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "owner_name") val ownerName: String,
    @ColumnInfo(name = "stars") val stars: Int,
    @ColumnInfo(name = "forks") val forks: Int,
    @ColumnInfo(name = "url") val url: String,
    @ColumnInfo(name = "page") val pagePosition: Int,
) {
    companion object {
        internal const val TABLE_NAME = "repositories"
    }
}
