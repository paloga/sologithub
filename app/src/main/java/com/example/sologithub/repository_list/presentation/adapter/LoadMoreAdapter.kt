package com.example.sologithub.repository_list.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sologithub.databinding.RowLoadStateBinding

class LoadMoreAdapter(private val retry: () -> Unit) : LoadStateAdapter<LoadMoreViewHolder>() {
    override fun onBindViewHolder(holder: LoadMoreViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadMoreViewHolder {
        return LoadMoreViewHolder.create(parent, retry)
    }
}

class LoadMoreViewHolder(
    private val binding: RowLoadStateBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.btnRetry.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        with(binding) {
            pbLoading.isVisible = loadState is LoadState.Loading
            btnRetry.isVisible = loadState is LoadState.Error
            txtErrorMessage.isVisible = !(loadState as? LoadState.Error)?.error?.message.isNullOrBlank()
            txtErrorMessage.text = (loadState as? LoadState.Error)?.error?.message
        }
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): LoadMoreViewHolder {
            val binding = RowLoadStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return LoadMoreViewHolder(binding, retry)
        }
    }
}