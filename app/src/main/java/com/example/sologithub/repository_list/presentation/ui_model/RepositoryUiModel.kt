package com.example.sologithub.repository_list.presentation.ui_model

data class RepositoryUiModel(
    val name: String,
    val description: String,
    val ownerName: String,
    val stars: String
)
