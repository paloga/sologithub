package com.example.sologithub.repository_list.data.datasource

import com.example.sologithub.common.database.SoloGithubDb
import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.common.tool.coroutines.CoroutineDispatchers
import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import com.example.sologithub.repository_list.data.database.RepositoryPageDto
import com.example.sologithub.repository_list.data.entity.RepositoryListEntity
import com.example.sologithub.repository_list.data.entity.mapper.toDto
import com.example.sologithub.repository_list.data.entity.mapper.toEntity
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RoomRepositoryListDataSource @Inject constructor(
    database: SoloGithubDb,
    private val dispatchers: CoroutineDispatchers,
) : RepositoryListDatasource {

    private val pageDao by lazy { database.getRepositoryPageDao() }
    private val repositoryDao by lazy { database.getRepositoryDao() }

    private suspend fun getCachedPageCache(pagePosition: Int): RepositoryPageDto? =
        pageDao.getPage(pagePosition)

    override suspend fun fetchRepositories(page: Int): SoloGithubResult<SoloGithubApiError, RepositoryListEntity> =
        withContext(dispatchers.io) {
            val cachedPage = getCachedPageCache(pagePosition = page)

            if (cachedPage != null && cachedPage.isValid()) {
                repositoryDao.getRepositoriesByPage(cachedPage.pagePosition).let { cachedRepositories ->
                    SoloGithubResult.Success(RepositoryListEntity(items = cachedRepositories.map { it.toEntity() }))
                }
            } else {
                SoloGithubResult.Success(RepositoryListEntity(items = emptyList()))
            }
        }

    suspend fun saveRepositories(page: Int, repositories: List<RepositoryEntity>) =
        withContext(dispatchers.io) {
            val pageDto = RepositoryPageDto(
                pagePosition = page,
                validToInMillis = System.currentTimeMillis() + CACHE_VALIDITY_TIME_IN_MILLIS
            )
            pageDao.insertPage(pageDto)
            repositoryDao.insertAll(
                repositories = repositories.map { it.toDto(page) }.toTypedArray()
            )
        }

    private fun RepositoryPageDto.isValid(): Boolean {
        return validToInMillis >= System.currentTimeMillis()
    }

    companion object {
        private const val CACHE_VALIDITY_TIME_IN_MILLIS = 1 * 60 * 1000 // 1 minute
    }

}