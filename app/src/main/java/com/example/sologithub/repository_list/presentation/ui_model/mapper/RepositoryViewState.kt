package com.example.sologithub.repository_list.presentation.ui_model.mapper

sealed class RepositoryViewState {
    data object Loading : RepositoryViewState()
    data object NoInternetError : RepositoryViewState()
    data object DefaultError : RepositoryViewState()
    data object Data : RepositoryViewState()
}
