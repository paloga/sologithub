package com.example.sologithub.repository_list.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.repository_list.domain.use_case.FetchRepositoryListUseCase
import com.example.sologithub.repository_list.presentation.ui_model.RepositoryUiModel
import com.example.sologithub.repository_list.presentation.ui_model.mapper.RepositoryViewState
import com.example.sologithub.repository_list.presentation.ui_model.mapper.toUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class RepositoryListViewModel @Inject constructor(
    private val fetchRepositoryList: FetchRepositoryListUseCase
) : ViewModel() {

    private val _navigateToRepositoryDetails: MutableStateFlow<RepositoryUiModel?> = MutableStateFlow(null)
    val navigateToRepositoryDetails: StateFlow<RepositoryUiModel?>
        get() = _navigateToRepositoryDetails

    private val _viewState: MutableStateFlow<RepositoryViewState> = MutableStateFlow(RepositoryViewState.Loading)
    val viewState: StateFlow<RepositoryViewState>
        get() = _viewState

    fun searchRepos(): Flow<PagingData<RepositoryUiModel>> =
        fetchRepositoryList().map { it.map { model -> model.toUiModel() } }.cachedIn(viewModelScope)

    fun onLoadedStateChanged(loadState: LoadState) {
        when (loadState) {
            is LoadState.Error -> {
                if (loadState.error is SoloGithubError.InternetNotAvailable) {
                    _viewState.value = RepositoryViewState.NoInternetError
                } else {
                    _viewState.value = RepositoryViewState.DefaultError
                }
            }
            is LoadState.Loading -> {
                _viewState.value = RepositoryViewState.Loading
            }
            is LoadState.NotLoading -> {
                _viewState.value = RepositoryViewState.Data
            }
        }

    }

    fun onRepositoryClicked(repository: RepositoryUiModel) {
        _navigateToRepositoryDetails.value = repository
    }

    fun onRepositoryDetailsNavigated() {
        _navigateToRepositoryDetails.value = null
    }
}