package com.example.sologithub.repository_list.data.entity.mapper

import com.example.sologithub.common.extensions.cleanDescription
import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import com.example.sologithub.repository_details.data.entity.RepositoryOwnerEntity
import com.example.sologithub.repository_list.data.database.RepositoryDto
import com.example.sologithub.repository_list.data.entity.RepositoryListEntity
import com.example.sologithub.repository_list.domain.model.RepositoryModel

fun RepositoryListEntity.toModel(): List<RepositoryModel> =
    items.map {
        RepositoryModel(
            name = it.name,
            description = it.description.cleanDescription(),
            ownerName = it.owner.name,
            stars = it.stars
        )
    }

fun RepositoryDto.toEntity(): RepositoryEntity =
    RepositoryEntity(
        name = name,
        description = description,
        owner = RepositoryOwnerEntity(ownerName),
        stars = stars,
        forks = forks,
        url = url
    )

fun RepositoryEntity.toDto(page: Int): RepositoryDto =
    RepositoryDto(
        name = name,
        description = description,
        ownerName = owner.name,
        stars = stars,
        forks = forks,
        url = url,
        pagePosition = page
    )
