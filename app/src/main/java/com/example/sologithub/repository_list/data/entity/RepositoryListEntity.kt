package com.example.sologithub.repository_list.data.entity

import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RepositoryListEntity(
    @SerialName("items") val items: List<RepositoryEntity>
)