package com.example.sologithub.repository_list.data.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.common.network.model.mapper.toModel
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.common.tool.flatMap
import com.example.sologithub.common.tool.map
import com.example.sologithub.common.tool.mapFailure
import com.example.sologithub.common.tool.onSuccess
import com.example.sologithub.common.tool.success
import com.example.sologithub.repository_list.data.datasource.ApiRepositoryListDataSource
import com.example.sologithub.repository_list.data.datasource.RoomRepositoryListDataSource
import com.example.sologithub.repository_list.data.entity.mapper.toModel
import com.example.sologithub.repository_list.domain.model.RepositoryModel
import com.example.sologithub.repository_list.domain.repository.RepositoryListRepository
import timber.log.Timber

class RepositoryListDataRepository(
    private val apiDataSource: ApiRepositoryListDataSource,
    private val roomDataSource: RoomRepositoryListDataSource,
) : RepositoryListRepository {

    private suspend fun fetchRepositories(page: Int): SoloGithubResult<SoloGithubError, List<RepositoryModel>> =
        roomDataSource.fetchRepositories(page)
            .flatMap { localRepositories ->
                if (localRepositories.items.isEmpty()) {
                    apiDataSource.fetchRepositories(page)
                        .onSuccess { remoteRepositories ->
                            roomDataSource.saveRepositories(page, remoteRepositories.items)
                        }
                } else {
                    localRepositories.success()
                }
            }
            .map { entity -> entity.toModel() }
            .mapFailure { apiError -> apiError.toModel() }

    override fun fetchPaginatedRepositories(): PagingSource<Int, RepositoryModel> {
        return object : PagingSource<Int, RepositoryModel>() {
            override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RepositoryModel> {
                val position = params.key ?: 1

                return try {
                    Timber.d("PABLOKU fetchRepositories position: $position")
                    val response = fetchRepositories(position)
                    if (response.isSuccess) {
                        val data = response.getOrNull() ?: emptyList()
                        val prevKey = if (position == 1) null else position - 1
                        val nextKey = if (data.isEmpty()) null else position + 1
                        LoadResult.Page(data, prevKey, nextKey)
                    } else {
                        val exception = response.exceptionOrNull() ?: Exception("Unknown error")
                        Timber.e(exception)
                        LoadResult.Error(exception)
                    }
                } catch (exception: Exception) {
                    Timber.e(exception)
                    LoadResult.Error(exception)
                }
            }

            override fun getRefreshKey(state: PagingState<Int, RepositoryModel>): Int? {
                return state.anchorPosition?.let { anchorPosition ->
                    state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                        ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
                }
            }
        }
    }
}