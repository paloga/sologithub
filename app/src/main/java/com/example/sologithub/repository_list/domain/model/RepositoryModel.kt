package com.example.sologithub.repository_list.domain.model

data class RepositoryModel(
    val name: String,
    val description: String,
    val ownerName: String,
    val stars: Int
)
