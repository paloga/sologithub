package com.example.sologithub.repository_list.domain.use_case

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.sologithub.repository_list.domain.repository.RepositoryListRepository
import javax.inject.Inject

class FetchRepositoryListUseCase @Inject constructor(
    private val repositoryListRepository: RepositoryListRepository
) {

    operator fun invoke() = Pager(
        PagingConfig(
            pageSize = 2,
            prefetchDistance = 30
        )
    ) {
        repositoryListRepository.fetchPaginatedRepositories()
    }.flow

}