package com.example.sologithub.repository_details.di

import com.example.sologithub.repository_details.data.datasource.ApiRepositoryDetailsDatasource
import com.example.sologithub.repository_details.data.repository.RepositoryDetailsDataRepository
import com.example.sologithub.repository_details.domain.repository.RepositoryDetailsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class RepositoryDetailsDataModule {

    @Provides
    @Singleton
    fun provideRepositoryDetailsRepository(
        apiDataSource: ApiRepositoryDetailsDatasource,
    ): RepositoryDetailsRepository = RepositoryDetailsDataRepository(apiDataSource)

}