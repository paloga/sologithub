package com.example.sologithub.repository_details.presentation.ui_model

import com.example.sologithub.common.presentation.ViewAction

sealed class RepositoryDetailsViewAction : ViewAction {
    data class OnOpenInBrowserClicked(val url: String) : RepositoryDetailsViewAction()
    data object OnBackClicked : RepositoryDetailsViewAction()
}