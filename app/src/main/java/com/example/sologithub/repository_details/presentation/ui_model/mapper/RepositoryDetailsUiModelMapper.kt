package com.example.sologithub.repository_details.presentation.ui_model.mapper

import com.example.sologithub.common.extensions.cleanDescription
import com.example.sologithub.repository_details.domain.model.RepositoryDetailsModel
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsUiModel

fun RepositoryDetailsModel.toUiModel() = RepositoryDetailsUiModel(
    name = name,
    description = description.cleanDescription(),
    stars = starsCount,
    forks = forksCount,
    primaryLanguage = primaryLanguage,
    url = url
)