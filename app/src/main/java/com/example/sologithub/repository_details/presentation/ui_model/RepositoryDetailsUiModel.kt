package com.example.sologithub.repository_details.presentation.ui_model

data class RepositoryDetailsUiModel(
    val name: String,
    val description: String,
    val stars: Int,
    val forks: Int,
    val primaryLanguage: String?,
    val url: String
)
