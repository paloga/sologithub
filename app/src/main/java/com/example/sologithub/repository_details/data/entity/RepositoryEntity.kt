package com.example.sologithub.repository_details.data.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RepositoryEntity(
    @SerialName("name") val name: String,
    @SerialName("owner") val owner: RepositoryOwnerEntity,
    @SerialName("description") val description: String,
    @SerialName("stargazers_count") val stars: Int,
    @SerialName("forks_count") val forks: Int,
    @SerialName("html_url") val url: String
)

@Serializable
data class RepositoryOwnerEntity(
    @SerialName("login") val name: String,
)