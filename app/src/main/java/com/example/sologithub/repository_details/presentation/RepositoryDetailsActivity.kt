package com.example.sologithub.repository_details.presentation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.example.sologithub.databinding.ActivityRepositoryDetailsBinding
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsUiModel
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewAction
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewEvent
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RepositoryDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRepositoryDetailsBinding
    private val viewModel: RepositoryDetailsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepositoryDetailsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        initToolbar()

        observeViewStates()
        observeViewEvents()
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.toolbar.setNavigationOnClickListener {
            viewModel.onViewAction(RepositoryDetailsViewAction.OnBackClicked)
        }
    }

    private fun observeViewStates() {
        lifecycleScope.launch {
            viewModel.viewState.collect { state ->
                when (state) {
                    is RepositoryDetailsViewState.WithData -> showData(repositoryDetails = state.data)
                    RepositoryDetailsViewState.Loading -> showLoading()
                    RepositoryDetailsViewState.Error -> showError()
                }
            }
        }
    }

    private fun observeViewEvents() {
        lifecycleScope.launch {
            viewModel.viewEvent.collect { event ->
                viewModel.consumeEvent(event) {
                    when (it) {
                        is RepositoryDetailsViewEvent.OpenInBrowser -> openInBrowser(it.url)
                        RepositoryDetailsViewEvent.NavigateBack -> finish()
                    }
                }
            }
        }
    }

    private fun showData(repositoryDetails: RepositoryDetailsUiModel) {
        with(binding) {
            showContent()
            pbLoading.hideWithAlphaAnimation()
            errorView.hideWithAlphaAnimation()

            txtName.text = repositoryDetails.name
            txtDescription.text = repositoryDetails.description
            txtPrimaryLanguage.isVisible = repositoryDetails.primaryLanguage != null
            txtPrimaryLanguage.text = repositoryDetails.primaryLanguage.orEmpty()
            txtStars.text = repositoryDetails.stars.toString()
            txtForks.text = repositoryDetails.forks.toString()

            btnOpenInBrowser.setOnClickListener {
                viewModel.onViewAction(RepositoryDetailsViewAction.OnOpenInBrowserClicked(repositoryDetails.url))
            }
        }
    }

    private fun View.showWithAlphaAnimation(delay: Long = 0) {
        ViewCompat.animate(this)
            .alpha(1f)
            .setStartDelay(delay)
            .start()
    }

    private fun View.hideWithAlphaAnimation(delay: Long = 0) {
        ViewCompat.animate(this)
            .alpha(0f)
            .setStartDelay(delay)
            .start()
    }

    private fun showLoading() {
        with(binding) {
            hideContent()
            pbLoading.showWithAlphaAnimation()
            errorView.hideWithAlphaAnimation()
        }
    }

    private fun showError() {
        with(binding) {
            hideContent()
            pbLoading.hideWithAlphaAnimation()
            errorView.showWithAlphaAnimation()
        }
    }

    private fun showContent() {
        with(binding) {
            txtName.showWithAlphaAnimation(100)
            txtDescription.showWithAlphaAnimation(200)
            txtPrimaryLanguage.showWithAlphaAnimation(300)
            txtStars.showWithAlphaAnimation(400)
            txtForks.showWithAlphaAnimation(400)
            btnOpenInBrowser.showWithAlphaAnimation(500)
        }
    }

    private fun hideContent() {
        with(binding) {
            txtName.hideWithAlphaAnimation()
            txtDescription.hideWithAlphaAnimation()
            txtPrimaryLanguage.hideWithAlphaAnimation()
            txtStars.hideWithAlphaAnimation()
            txtForks.hideWithAlphaAnimation()
            btnOpenInBrowser.hideWithAlphaAnimation()
        }
    }

    private fun openInBrowser(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    companion object {
        const val ARG_OWNER_NAME = "owner_name"
        const val ARG_REPOSITORY_NAME = "repository_name"

        fun build(context: Context, ownerName: String, repositoryName: String): Intent =
            Intent(context, RepositoryDetailsActivity::class.java).apply {
                putExtra(ARG_OWNER_NAME, ownerName)
                putExtra(ARG_REPOSITORY_NAME, repositoryName)
            }
    }
}