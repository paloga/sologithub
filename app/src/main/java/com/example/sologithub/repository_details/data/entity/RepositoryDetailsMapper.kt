package com.example.sologithub.repository_details.data.entity

import com.example.sologithub.repository_details.domain.model.RepositoryDetailsModel

fun RepositoryEntity.toModel(primaryLanguage: String?) = RepositoryDetailsModel(
    name = name,
    description = description,
    starsCount = stars,
    forksCount = forks,
    primaryLanguage = primaryLanguage,
    url = url
)