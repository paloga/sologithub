package com.example.sologithub.repository_details.data.datasource

import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import com.example.sologithub.repository_details.data.entity.RepositoryLanguageEntity

interface RepositoryDetailsDatasource {

    suspend fun getRepositoryDetails(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubApiError, RepositoryEntity>

    suspend fun getRepositoryLanguages(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubApiError, List<RepositoryLanguageEntity>>
}