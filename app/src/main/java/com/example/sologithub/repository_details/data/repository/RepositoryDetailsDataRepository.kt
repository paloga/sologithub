package com.example.sologithub.repository_details.data.repository

import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.common.network.model.mapper.toModel
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.common.tool.flatMap
import com.example.sologithub.common.tool.map
import com.example.sologithub.common.tool.mapFailure
import com.example.sologithub.repository_details.data.datasource.ApiRepositoryDetailsDatasource
import com.example.sologithub.repository_details.data.entity.toModel
import com.example.sologithub.repository_details.domain.model.RepositoryDetailsModel
import com.example.sologithub.repository_details.domain.repository.RepositoryDetailsRepository

class RepositoryDetailsDataRepository(
    private val apiDataSource: ApiRepositoryDetailsDatasource
) : RepositoryDetailsRepository {

    override suspend fun getRepositoryDetails(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubError, RepositoryDetailsModel> {
        return apiDataSource.getRepositoryDetails(ownerName, repositoryName)
            .flatMap { repositoryDetailsEntity ->
                apiDataSource.getRepositoryLanguages(ownerName, repositoryName)
                    .map { languages -> repositoryDetailsEntity.toModel(languages.firstOrNull()?.name) }
            }
            .mapFailure { apiError -> apiError.toModel() }
    }
}