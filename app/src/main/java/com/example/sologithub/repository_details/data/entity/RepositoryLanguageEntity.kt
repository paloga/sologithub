package com.example.sologithub.repository_details.data.entity

data class RepositoryLanguageEntity(
    val name: String,
    val bytesOfCode: Int
)
