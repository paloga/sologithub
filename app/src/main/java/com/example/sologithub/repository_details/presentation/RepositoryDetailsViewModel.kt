package com.example.sologithub.repository_details.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sologithub.common.presentation.ViewModelActions
import com.example.sologithub.common.presentation.ViewModelEvents
import com.example.sologithub.common.presentation.ViewModelEventsImpl
import com.example.sologithub.common.presentation.ViewModelStates
import com.example.sologithub.common.presentation.ViewModelStatesImpl
import com.example.sologithub.common.tool.onFailure
import com.example.sologithub.common.tool.onSuccess
import com.example.sologithub.repository_details.domain.use_case.GetRepositoryDetailsUseCase
import com.example.sologithub.repository_details.presentation.RepositoryDetailsActivity.Companion.ARG_OWNER_NAME
import com.example.sologithub.repository_details.presentation.RepositoryDetailsActivity.Companion.ARG_REPOSITORY_NAME
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsUiModel
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewAction
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewEvent
import com.example.sologithub.repository_details.presentation.ui_model.RepositoryDetailsViewState
import com.example.sologithub.repository_details.presentation.ui_model.mapper.toUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RepositoryDetailsViewModel @Inject constructor(
    private val getRepositoryDetails: GetRepositoryDetailsUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel(),
    ViewModelStates<RepositoryDetailsViewState> by ViewModelStatesImpl(RepositoryDetailsViewState.Loading),
    ViewModelActions<RepositoryDetailsViewAction>,
    ViewModelEvents<RepositoryDetailsViewEvent> by ViewModelEventsImpl() {

    private val ownerName: String = savedStateHandle[(ARG_OWNER_NAME)] ?: ""
    private val repositoryName: String = savedStateHandle[(ARG_REPOSITORY_NAME)] ?: ""

    init {
        launchGetRepositoryDetails()
    }

    private fun launchGetRepositoryDetails() {
        updateState(RepositoryDetailsViewState.Loading)
        viewModelScope.launch {
            getRepositoryDetails(ownerName, repositoryName)
                .onSuccess { repositories ->
                    onGetRepositoryDetailsSuccess(repositories.toUiModel())
                }
                .onFailure {
                    onGetRepositoryDetailsFailure()
                }
        }
    }

    private fun onGetRepositoryDetailsSuccess(repositories: RepositoryDetailsUiModel) {
        updateState(RepositoryDetailsViewState.WithData(repositories))
    }

    private fun onGetRepositoryDetailsFailure() {
        updateState(RepositoryDetailsViewState.Error)
    }

    override fun onViewAction(action: RepositoryDetailsViewAction) {
        when (action) {
            is RepositoryDetailsViewAction.OnOpenInBrowserClicked ->
                addEvent(RepositoryDetailsViewEvent.OpenInBrowser(action.url))

            RepositoryDetailsViewAction.OnBackClicked -> addEvent(RepositoryDetailsViewEvent.NavigateBack)
        }
    }
}