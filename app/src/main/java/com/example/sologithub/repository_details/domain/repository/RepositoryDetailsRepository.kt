package com.example.sologithub.repository_details.domain.repository

import com.example.sologithub.common.domain.model.SoloGithubError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.repository_details.domain.model.RepositoryDetailsModel

interface RepositoryDetailsRepository {

    suspend fun getRepositoryDetails(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubError, RepositoryDetailsModel>

}