package com.example.sologithub.repository_details.presentation.ui_model

import com.example.sologithub.common.presentation.ViewEvent

sealed class RepositoryDetailsViewEvent : ViewEvent {
    data class OpenInBrowser(val url: String) : RepositoryDetailsViewEvent()
    data object NavigateBack : RepositoryDetailsViewEvent()
}