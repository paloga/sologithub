package com.example.sologithub.repository_details.presentation.ui_model

import com.example.sologithub.common.presentation.ViewState

sealed class RepositoryDetailsViewState : ViewState {

    data object Loading : RepositoryDetailsViewState()

    data object Error : RepositoryDetailsViewState() // TODO [Pabloku] Separate error from no internet error

    data class WithData(val data: RepositoryDetailsUiModel) : RepositoryDetailsViewState()

}