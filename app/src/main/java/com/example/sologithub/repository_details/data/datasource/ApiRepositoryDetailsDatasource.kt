package com.example.sologithub.repository_details.data.datasource

import com.example.sologithub.common.network.SoloGithubApi
import com.example.sologithub.common.network.model.SoloGithubApiError
import com.example.sologithub.common.tool.SoloGithubResult
import com.example.sologithub.common.tool.map
import com.example.sologithub.repository_details.data.entity.RepositoryEntity
import com.example.sologithub.repository_details.data.entity.RepositoryLanguageEntity
import javax.inject.Inject

class ApiRepositoryDetailsDatasource @Inject constructor(private val api: SoloGithubApi) : RepositoryDetailsDatasource {

    override suspend fun getRepositoryDetails(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubApiError, RepositoryEntity> =
        api.getRepositoryDetails(owner = ownerName, name = repositoryName)

    override suspend fun getRepositoryLanguages(
        ownerName: String,
        repositoryName: String
    ): SoloGithubResult<SoloGithubApiError, List<RepositoryLanguageEntity>> =
        api.getRepositoryLanguages(owner = ownerName, name = repositoryName)
            .map {
                it.map { (name, bytesOfCode) ->
                    RepositoryLanguageEntity(name = name, bytesOfCode = bytesOfCode.toInt())
                }
            }
}