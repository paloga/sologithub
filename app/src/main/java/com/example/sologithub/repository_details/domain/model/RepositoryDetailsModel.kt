package com.example.sologithub.repository_details.domain.model

data class RepositoryDetailsModel(
    val name: String,
    val description: String,
    val starsCount: Int,
    val forksCount: Int,
    val primaryLanguage: String?,
    val url: String
)
